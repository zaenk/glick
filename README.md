This is a wrapper around GitLab CLI, specifically to make it easier to
work with ci/cd variables.

With a setup, where CI/CD variables are used for storing project secrets,
it is difficult to make updates to all these variables at once.

With the change of the GitLab UI (from modal to sidebar) the UX also
does not support effectively or rather quickly edit these variables.

Features:
- while in a project directory, list ci/cd variables related to that project
- switch between grouping by name or environment
- edit grouped variables, using the `EDITOR`. this will be a multi document yaml file, each document is a single variable

Goals:
- learn some go
- learn some [bubbletea](https://github.com/charmbracelet/bubbletea) for presenting options
- learn some [lipgloss](https://github.com/charmbracelet/lipgloss) maybe

TODOs:
- [ ] list variables of a project
- [ ] update a single variable
- [ ] detect if the variable is a k8s secret, apply base64 encode/decode automatically
- [ ] edit multiple variables with the same name
- [ ] edit multiple variables with the same environment
